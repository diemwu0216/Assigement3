var app = angular.module('myApp',[])


// define Loacal storage
app.factory('$localstorage', ['$window', function($window) {
  return {
    // set object storage
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    // get object storage
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);

// Login & Register controller
app.controller('indexController', function($scope, $http,$location, $window,$log,$localstorage) {
  
  /* an array to store the list items */
  var items = Array()
    $scope.userName; //user login Name
    $scope.pw = "";  //user pw
    $scope.tab = 1;  // default login page
    $scope.email;    //user email

   $scope.setTab = function(newTab){ //set Tab
            $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){ //to tab
      return $scope.tab === tabNum;
  };

  // Login function
  $scope.login = function(){

  // User object 
      var userObj = {
        UserName : $scope.userName,
        UserPw : $scope.pw
    };

    var size = $scope.pw.length;
    if (size < 6) {
      $scope.strength = 'weak';
      alert("password should be >= 6")
      return;
    }


  // API Login URL
    var url = "http://port-8080.nodejs-diemwu0216162163.codeanyapp.com/login";
    $http.post(url,JSON.stringify(userObj),{headers: {'Content-Type': 'application/json'} }).success(function(data) {

        if(data.message == 'user found'){
          $scope.status = 's';
          console.log("Login Successful")
              var path = "home.html";
              $localstorage.set('name', $scope.userName);
              window.location.href = path;
        }else{
                    $scope.status = 'f';
          console.log(' fail login');
          alert("can't find user")
        }

    }).error(function(data) {
                          $scope.status = 'f';

        alert("ERROR" + JSON.stringify(data));
    });




  }

// Register function
  $scope.register = function(){
    // Register Object 
    var userObj = {
        UserName : $scope.userName,
        UserPw : $scope.pw,
        email: $scope.email
    };

    var size = $scope.pw.length;
    if (size < 6) {
      $scope.strength = 'weak';
      // alert("password should be >= 6")
      return; 
    }

    var url = "http://port-8080.nodejs-diemwu0216162163.codeanyapp.com/addUser";  // API Register URL

    $http.post(url,JSON.stringify(userObj),{headers: {'Content-Type': 'application/json'} }).success(function(data) {
        if(data.message =="new users added"){ // Register success
           var path = "home.html";
              $localstorage.set('name', $scope.userName);
              window.location.href = path;
              console.log(' Register Successful')
        }

    }).error(function(data) {
        alert("ERROR" + JSON.stringify(data));
    });

  }
}) //end of index controller

app.controller('homeController', function($scope, $http,$scope,$rootScope,$localstorage,$filter) {
  

// Define All value
  var items = Array();
  $scope.userName = $localstorage.get('name');
  $scope.country ="",
  $scope.dariy ="";
  $scope.temp ="";
  $scope.main ="";
  $scope.title ="";
  $scope.content ="";
  $scope.weather;
  $scope.diarys;
  $scope.date = $filter('date')(new Date(),'yyyy-MM-dd');
  $scope.search_date;


alert("Weclome "+ $scope.userName); // Weclome alert
  $scope.initData = function(){ //init Data
    $scope.getWeather();
    $scope.getDiary();
  }

  $scope.getWeather = function(){ // get weather hk information
    
    var url = "http://port-8080.nodejs-diemwu0216162163.codeanyapp.com/weather/hong kong";
      $http.get(url).success(function(response) {
        console.log("getWeather Successful");
        console.log(response)
        $scope.weather = response.data;
        // $scope.books = response.items
        // $scope.searchTerm = ''
      })
    }

  

  $scope.getDiary =function(){ // get diary by user name
     var url = "http://port-8080.nodejs-diemwu0216162163.codeanyapp.com/diary/"+$scope.userName;
      $http.get(url).success(function(response) {
        console.log("get Diary Successful");
        console.log(response)
        $scope.diarys = response.data;
        // $scope.books = response.items
        // $scope.searchTerm = ''
      })
    }
  

  $scope.addDiary =function(){ // add Diary

        if($scope.content == ''){
      $scope.check_cont_null = "isNull";
      return
    }
     if($scope.title == ''){
      $scope.check_title_null = "isNull";
      return
    }

    // Diary object
        var diaryObj = {
        UserName : $scope.userName,
        Country : $scope.weather.name,
        Date : $scope.date,
        Weather :{ 
          temp: $scope.weather.weather[0].description,
          main: $scope.weather.weather[0].main
        },
        Title: $scope.title,
        content : $scope.content
    };




    // API add Diray
    var url = "http://port-8080.nodejs-diemwu0216162163.codeanyapp.com/addDiary";

    $http.post(url,JSON.stringify(diaryObj),{headers: {'Content-Type': 'application/json'} }).success(function(data) {
        $scope.getDiary();

    }).error(function(data) {
        alert("ERROR" + JSON.stringify(data));
    });

  }

  $scope.delete_diary =function(item, index){ // delete Diary
        // API add Diray by object id
    var url = "http://port-8080.nodejs-diemwu0216162163.codeanyapp.com/diary_delete/"+item._id;
      $http.get(url).success(function(response) {
        console.log("response"+JSON.stringify(response))
        if(response.message =="deleted"){
          alert("item diary deleted");
               }
      $scope.getDiary();
         console.log($scope.Search_diarys)
           $scope.Search_diarys.splice(index, 1); 

    })
  }

// API search Diray
  $scope.search_diart=function(){
     var url = "http://port-8080.nodejs-diemwu0216162163.codeanyapp.com/search_diary/"+$scope.userName+"/"+$scope.search_date;
      $http.get(url).success(function(response) {
        $scope.Search_diarys = response.data;
      })
  }
})